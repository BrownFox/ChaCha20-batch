@echo off
setlocal enabledelayedexpansion
for /f "delims==" %%a in ('set') do set %%a=

REM RFC-7539 CHACHA20 STREAM CIPHER

echo enter 256 bit key in hexadecimal notation
set /p key=key 
echo enter 96 bit nonce in hexadecimal notation
set /p nonce=nonce 
echo enter starting block
set /p sblock=sblock 
echo enter ending block
set /p eblock=eblock 

echo generating keystream...

for /l %%a in (%sblock%,1,%eblock%) do (

    set blockcounter=%%a

    pause

    call :resetstate

    for /l %%a in (0,1,15) do (
        set workingstate%%a=!state%%a!
    )

    for /l %%a in (1,1,10) do (
        call :quarterround 0  4  8 12
        call :quarterround 1  5  9 13
        call :quarterround 2  6 10 14
        call :quarterround 3  7 11 15
        call :quarterround 0  5 10 15
        call :quarterround 1  6 11 12
        call :quarterround 2  7  8 13
        call :quarterround 3  4  9 14
    )

    echo Keystream of block !blockcounter!

    for /l %%a in (0,1,15) do (
    	set /a state%%a+=!workingstate%%a!
        set /a "state%%a=((state%%a>>24)&0xff)|((state%%a>>8)&0xff00)|((state%%a<<8)&0xff0000)|((state%%a<<24)&0xff000000)"
    	call :printhex !state%%a!
    )

)

pause

exit/b
:quarterround
set v1=!workingstate%1!
set v2=!workingstate%2!
set v3=!workingstate%3!
set v4=!workingstate%4!

set /a v1+=v2
set /a v4^^=v1
set /a "left=v4<<16"
set /a "right=v4>>16"
set /a "v4=left|(right&65535)"
set /a v3+=v4
set /a v2^^=v3
set /a "left=v2<<12"
set /a "right=v2>>20"
set /a "v2=left|(right&4095)"
set /a v1+=v2
set /a v4^^=v1
set /a "left=v4<<8"
set /a "right=v4>>24"
set /a "v4=left|(right&255)"
set /a v3+=v4
set /a v2^^=v3
set /a "left=v2<<7"
set /a "right=v2>>25"
set /a "v2=left|(right&127)"

set workingstate%1=%v1%
set workingstate%2=%v2%
set workingstate%3=%v3%
set workingstate%4=%v4%

exit /b

:resetstate
set /a state0=0x61707865
set /a state1=0x3320646e
set /a state2=0x79622d32
set /a state3=0x6b206574
set /a state4=0x%key:~0,8%
set /a state5=0x%key:~8,8%
set /a state6=0x%key:~16,8%
set /a state7=0x%key:~24,8%
set /a state8=0x%key:~32,8%
set /a state9=0x%key:~40,8%
set /a state10=0x%key:~48,8%
set /a state11=0x%key:~56,8%
set /a state12=%blockcounter%
set /a state13=0x%nonce:~0,8%
set /a state14=0x%nonce:~8,8%
set /a state15=0x%nonce:~16,8%
for %%a in (4 5 6 7 8 9 10 11 13 14 15) do (
    set /a "state%%a=((state%%a>>24)&0xff)|((state%%a>>8)&0xff00)|((state%%a<<8)&0xff0000)|((state%%a<<24)&0xff000000)"
)
exit/b

:printbin
  setlocal enableDelayedExpansion
  set n=%~1
  set rtn=
  for /l %%b in (0,1,31) do (
    set /a "d=n&1, n>>=1"
    set rtn=!d!!rtn!
  )
  (endlocal & rem -- return values
    echo %rtn%
  )
exit /b

:printhex
set /a dec=%~1
set "hex="
set "map=0123456789ABCDEF"
for /L %%N in (1,1,8) do (
    set /a "d=dec&15,dec>>=4"
    for %%D in (!d!) do set "hex=!map:~%%D,1!!hex!"
)
echo %hex%
exit/b